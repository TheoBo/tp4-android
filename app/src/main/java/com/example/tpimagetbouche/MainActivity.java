package com.example.tpimagetbouche;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileNotFoundException;

import static java.lang.StrictMath.max;
import static java.lang.StrictMath.min;

public class MainActivity extends AppCompatActivity {
    public final static int REQUEST_CODE = 420;
    private Bitmap originalImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView image = findViewById(R.id.Image);
        registerForContextMenu(image);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
                MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menuprincipal, menu);
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.imagecontext, menu);
    }


    @Override
    public boolean onOptionsItemSelected (MenuItem item)
    {
        int id = item.getItemId();

        switch (id){
            case R.id.mirroirHorizontal :
                horizontalMirroring();
                break;

            case R.id.mirroirVertical :
                verticalMirroring();
                break;

            case R.id.clockwiseRot :
                clockwiseRotation();
                break;

            case R.id.counterClockwiseRot :
                counterClockwiseRotation();
                break;
        }
        return true;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        int id = item.getItemId();

        switch (id){
            case R.id.inverserCouleurs :
                invertColors();
                break;

            case R.id.convertirGris :
                convertToGray();
                break;

            case R.id.convertirGris2 :
                convertToGray2();
                break;

            case R.id.convertirGris3 :
                convertToGray3();
                break;

        }
        return true;
    }

    public void load(View monBoutton){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_CODE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = data.getData();
            //  ----- preparer les options de chargement de l’image
            BitmapFactory.Options option = new BitmapFactory.Options();
            option.inMutable = true; // l’image pourra etre modifiee
            // ------ chargement de l’image - valeur retournee null en cas d’erreur
            try {
                Bitmap bm = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri), null, option);
                TextView uriText = findViewById(R.id.URI);
                ImageView image = findViewById(R.id.Image);
                uriText.setText(imageUri.toString());
                image.setImageBitmap(bm);
                originalImage = bm;
            }catch (FileNotFoundException e){}
        }
    }

    //TODO: factoriser toutes les fonctions

    public void verticalMirroring(){
        ImageView image = findViewById(R.id.Image);
        if(image.getDrawable() == null){
            return;
        }
        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();
        Bitmap newBitmap = Bitmap.createBitmap(width, height,bitmap.getConfig());
        for (int i = 0; i < width; ++i){
            for (int j = 0; j < height; ++j){
                newBitmap.setPixel(width - 1 - i, j, bitmap.getPixel(i,j));
            }
        }
        image.setImageBitmap(newBitmap);
    }

    public void horizontalMirroring(){
        ImageView image = findViewById(R.id.Image);
        if(image.getDrawable() == null){
            return;
        }
        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();
        Bitmap newBitmap = Bitmap.createBitmap(width, height,bitmap.getConfig());
        for (int i = 0; i < width; ++i){
            for (int j = 0; j < height; ++j){
                newBitmap.setPixel(i, height - 1 - j, bitmap.getPixel(i,j));
            }
        }
        image.setImageBitmap(newBitmap);
    }


    public void invertColors() {
        ImageView image = findViewById(R.id.Image);
        if(image.getDrawable() == null){
            return;
        }
        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();
        Bitmap newBitmap = Bitmap.createBitmap(width, height,bitmap.getConfig());
        for (int i = 0; i < width; ++i){
            for (int j = 0; j < height; ++j){
                int pixel = bitmap.getPixel(i, j);
                int red = 255 - Color.red(pixel);
                int green = 255 - Color.green(pixel);
                int blue = 255 - Color.blue(pixel);

                newBitmap.setPixel(i, j, Color.argb(Color.alpha(pixel), red, green, blue));
            }
        }
        image.setImageBitmap(newBitmap);
    }

    public void convertToGray(){
        ImageView image = findViewById(R.id.Image);
        if(image.getDrawable() == null){
            return;
        }
        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();
        Bitmap newBitmap = Bitmap.createBitmap(width, height,bitmap.getConfig());
        for (int i = 0; i < width; ++i){
            for (int j = 0; j < height; ++j){
                int pixel = bitmap.getPixel(i, j);
                int red = Color.red(pixel);
                int green = Color.green(pixel);
                int blue =  Color.blue(pixel);
                int gray = (red + green + blue) / 3;
                newBitmap.setPixel(i, j, Color.argb(Color.alpha(pixel), gray, gray, gray));
            }
        }
        image.setImageBitmap(newBitmap);
    }

    public void convertToGray2(){
        ImageView image = findViewById(R.id.Image);
        if(image.getDrawable() == null){
            return;
        }
        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();
        Bitmap newBitmap = Bitmap.createBitmap(width, height,bitmap.getConfig());
        for (int i = 0; i < width; ++i){
            for (int j = 0; j < height; ++j){
                int pixel = bitmap.getPixel(i, j);
                int red = Color.red(pixel);
                int green = Color.green(pixel);
                int blue =  Color.blue(pixel);
                //gray = (max(RGB) + min(RGB)) / 2
                int gray = (max(red,max(green,blue)) + min(red, min(green, blue))) / 2;
                newBitmap.setPixel(i, j, Color.argb(Color.alpha(pixel), gray, gray, gray));
            }
        }
        image.setImageBitmap(newBitmap);
    }

    public void convertToGray3(){
        ImageView image = findViewById(R.id.Image);
        if(image.getDrawable() == null){
            return;
        }
        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();
        Bitmap newBitmap = Bitmap.createBitmap(width, height,bitmap.getConfig());
        for (int i = 0; i < width; ++i){
            for (int j = 0; j < height; ++j){
                int pixel = bitmap.getPixel(i, j);
                int red = Color.red(pixel);
                int green = Color.green(pixel);
                int blue =  Color.blue(pixel);
                //gray = 0.21R + 0.72G + 0.07B
                int gray = (int) (0.21 * red + 0.72 * green + 0.07 * blue);
                newBitmap.setPixel(i, j, Color.argb(Color.alpha(pixel), gray, gray, gray));
            }
        }
        image.setImageBitmap(newBitmap);
    }

    public void clockwiseRotation(){
        ImageView image = findViewById(R.id.Image);
        if(image.getDrawable() == null){
            return;
        }
        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();
        //invert the dimensions
        Bitmap newBitmap = Bitmap.createBitmap(height, width,bitmap.getConfig());
        for (int i = 0; i < width; ++i){
            for (int j = 0; j < height; ++j){
                newBitmap.setPixel(height - 1 - j, i, bitmap.getPixel(i,j));
            }
        }
        image.setImageBitmap(newBitmap);
    }

    public void counterClockwiseRotation(){
        ImageView image = findViewById(R.id.Image);
        if(image.getDrawable() == null){
            return;
        }
        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();
        //invert the dimensions
        Bitmap newBitmap = Bitmap.createBitmap(height, width,bitmap.getConfig());
        for (int i = 0; i < width; ++i){
            for (int j = 0; j < height; ++j){
                newBitmap.setPixel(j, width -1 - i, bitmap.getPixel(i,j));
            }
        }
        image.setImageBitmap(newBitmap);
    }

    public void restoreImage(View monBoutton) {
        ImageView image = findViewById(R.id.Image);

        if(originalImage == null ){
            return;
        }

        image.setImageBitmap(originalImage);
    }
}
